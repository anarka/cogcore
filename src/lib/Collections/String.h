#pragma once
#include <cstring>
#include <stdint.h>
#include <CogCore/Vector.h>


namespace Cog::Collections
{
  class String
  {
  public:
    String(const char* inLiteral) 
    {
      auto stringSize = strlen(inLiteral) + 1;
      isSmall = stringSize < sizeof(Vector<char>);

      if(isSmall)
      {
        memcpy(myString.mySmall, inLiteral, stringSize);
      }
      else
      {
        new (&myString.myLarge) Vector<char>();
        myString.myLarge.resize(stringSize);
        memcpy(myString.myLarge.data(), inLiteral, stringSize);
      }
    }

    operator const char*() const
    {
      if (isSmall)
      {
        return &myString.mySmall[0];
      }
      else
      {
        return myString.myLarge.data();
      }
    }

    ~String()
    {
      if (!isSmall)
      {
        myString.myLarge.~Vector<char>();
      }
    }

  private:
    bool isSmall{};
    union SmallString
    {
      Vector<char> myLarge; 
      char mySmall[sizeof(Vector<char>)];
      SmallString() {memset(this, 0, sizeof(SmallString));}
      ~SmallString() {}
    };

    SmallString myString{};
  };
};
