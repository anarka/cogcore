#pragma once
#include <cstring>
#include <string>
#include <vector>
#include <CogCore/Logger.h>

namespace Cog
{
  class Driver
  {
  public:
    Driver(int numArgs, char** args) :
      myAppName{args[0]}
    {
      if(numArgs == 1)
      {
        myReturnValue = CogMain();
      }
      else
      {
        std::vector<std::string> strArgs{};

        for(int i = 1; i < numArgs; i++)
        {
          strArgs.emplace_back(args[i]);
        }

        strArgs.push_back("");

        myReturnValue = CogMain(strArgs);
      }
    }

    int ReturnValue() const
    {
      return myReturnValue;
    }

  private:
      int CogMain(const std::vector<std::string>& args = {}) const
      {
        Logging::ColorLog("Starting app %s", Logging::Colors::GREEN, myAppName);

        try
        {
          return UserCogMain(args);
        }
        catch(std::runtime_error& e)
        {
          Logging::ErrorLog("CogMain threw fatal exception %s", e.what());
          return 127;
        }
      }

      static int UserCogMain(const std::vector<std::string>& args);

      std::string myAppName{};
      int myReturnValue{};
  };

  template<typename Metadata>
  class Entrypoint
  {
  public:
    static constexpr auto ARGUMENT = Metadata::ARGUMENT;
    
  private:
    
  };
}

#define CogMain \
int main(int argc, char** args)\
{\
  Cog::Driver mainDriver{argc, args};\
  return mainDriver.ReturnValue();\
}\
namespace Cog { \
int Driver::UserCogMain(const std::vector<std::string>& args)\
{

#define EndMain \
    return 0;\
  } \
}
