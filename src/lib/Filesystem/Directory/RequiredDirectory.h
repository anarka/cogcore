#pragma once
#include <CogCore/File.h>
#include <CogCore/Directory.h>

namespace Cog::Filesystem
{
  class RequiredDirectory : public File
  {
  public:
    RequiredDirectory(const std::string_view& path = "")
      : File{path}
    {
      if(!path.empty())
      {
        Directory::EnsureExists(Path());
      }
    }

    RequiredDirectory& operator=(const std::string_view& path)
    {
      UpdatePath(path);
      if(!path.empty())
      {
        Directory::EnsureExists(Path());
      }

      return *this;
    }
  };
}
