#pragma once

#include <string_view>
#include <filesystem>
#include <optional>
#include <CogCore/Vector.h>

#include <CogCore/File.h>

#include <CogCore/Logger.h>

namespace Cog::Filesystem
{ 
  class ExistingDirectory
  {
  public:
    using file_t = File;

    ExistingDirectory(std::string path);

    std::optional<file_t> LookupFilename(const std::string& toFind) const;

    template<typename Func, typename Pred>
    void ForEveryEntryIf(Func&& func, Pred&& pred)
    {
      for(auto&& subDir : m_subdirs)
      {
        subDir.ForEveryEntryIf(func, pred);
      }

      for(auto&& entry : m_files)
      {
        if(pred(entry))
        {
          func(entry);
        }
      }
    }

    template<typename Func, typename Pred>
    void ForEveryEntryIf(Func&& func, Pred&& pred) const
    {
      for(auto&& subDir : m_subdirs)
      {
        subDir.ForEveryEntryIf(func, pred);
      }

      for(auto&& entry : m_files)
      {
        if(pred(entry))
        {
          func(entry);
        }
      }
    }

    template<typename Func>
    void ForEveryEntry(Func&& func)
    {
      for(auto&& subDir : m_subdirs)
      {
        subDir.ForEveryEntry(func);
      }

      for(auto&& entry : m_files)
      {
        func(entry);
      }
    }

    template<typename Func>
    void ForEveryEntry(Func&& func) const
    {
      for(auto&& subDir : m_subdirs)
      {
        subDir.ForEveryEntry(func);
      }

      for(auto&& entry : m_files)
      {
        func(entry);
      }
    }

    template<typename Func>
    void ForEveryDir(Func&& func)
    {
      for(auto&& subDir : m_subdirs)
      {
        subDir.ForEveryDir(func);
      }

      func(m_root);
    }

    template<typename Func>
    void ForEveryDir(Func&& func) const
    {
      for(auto&& subDir : m_subdirs)
      {
        subDir.ForEveryDir(func);
      }

      func(m_root);
    }

    template<typename Func>
    void ForEveryToplevelSubdir(Func&& func)
    {
      for(auto&& subDir : m_subdirs)
      {
        func(subDir);
      }
    }

    template<typename Func>
    void ForEveryToplevelSubdir(Func&& func) const
    {
      for(auto&& subDir : m_subdirs)
      {
        func(subDir);
      }
    }

    template<typename Predicate>
    Collections::Vector<file_t> CollectFilesIf(Predicate&& pred) const
    {
      Collections::Vector<file_t> out{};
      for(auto&& file : m_files)
      {
        if(pred(file))
        {
          out.emplace_back(file);
        }
      }

      for(auto&& subdir : m_subdirs)
      {
        auto files = subdir.CollectFilesIf(std::forward<Predicate>(pred));
        out.reserve(out.size() + files.size());
        out.insert(out.end(), std::make_move_iterator(files.begin()), std::make_move_iterator(files.end()));
      }

      return out;
    }

    Collections::Vector<file_t> CollectFilesByExt(const std::string_view& ext) const;

    file_t AsFile() const
    {
      return m_root;
    };

    private:
      file_t m_root;
      Collections::Vector<ExistingDirectory> m_subdirs{};
      Collections::Vector<file_t> m_files{};
  };

  constexpr auto NO_FUNC = [](){};
  
  class Directory
  {
    class WorkingDirectory;
  public:
    static void EnsureExists(const File& file)
    {
      EnsureExists(file.Path());
    }
    static void EnsureExists(const std::string& path);

    template<typename PathType, typename Func = decltype(NO_FUNC), typename...Args>
    static auto WorkIn(PathType&& path, Func func = NO_FUNC, Args&&... args)
    {
      if constexpr(std::is_same_v<decltype(NO_FUNC), Func>)
      {
        (void)func;
        return WorkingDirectory(path);
      }
      else
      {
        auto workingDir = WorkIn(path);
        func(std::forward<Args>(args)...);
        return workingDir;
      }
    }
  
    template<typename Func, typename...Args>
    static WorkingDirectory SetupNew(const File& newDir, Func&& func, Args&&... args)
    {
      if(newDir.Exists())
      {
        throw std::runtime_error("SetupNewDir called for existing directory!");
      }

      try
      {
        return WorkIn(newDir, std::forward<Func>(func), std::forward<Args>(args)...);
      }
      catch(std::exception& e)
      {
        Logging::ErrorLog("Failed to SetupNewDir: %s cleaning up remnants.", newDir);
        std::filesystem::remove_all(newDir.Path());
        throw;
      }
    }

    static std::string WorkingDir();

  private:
    class WorkingDirectory
    {
    public:
      WorkingDirectory(const File& dirFile) : WorkingDirectory(dirFile.Path()) {};

      WorkingDirectory(const std::string& dirName)
      {
        EnsureExists(dirName);
        std::filesystem::current_path(dirName);
      }

      ~WorkingDirectory()
      {
        std::filesystem::current_path(m_cachedPath);
      }

    private:
      std::string m_cachedPath{WorkingDir()};
    };
  };
}
