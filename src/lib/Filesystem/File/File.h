#pragma once

#include <filesystem>
#include <vector>
#include <cstddef>
#include <fstream>
#include <CogCore/Logger.h>
#include <CogCore/FunctionWrappers.h>

namespace Cog::Filesystem
{

  class File
  {
  public:
    using timestamp_t = std::filesystem::file_time_type;
    File() = default;
    File(std::filesystem::directory_entry&&);
    File(std::string_view);

    std::string Path() const;
    std::string Name() const;
    std::string Ext() const;
    std::string Fullname() const;
    Filesystem::File Location() const;


    File WithExt(const File& newExt) const;
    File LocatedAt(const File& newLoc) const;
    File BasedAt(const File& newBase) const;
    
    File WithExt(const std::string_view& newExt) const;
    File LocatedAt(const std::string_view& newLoc) const;
    File BasedAt(const std::string_view& newBase) const;
    
    std::filesystem::file_time_type Timestamp() const;

    bool Exists() const;

    size_t Size() const;

    std::ofstream AsWriteableStream() const;

    class Hashor
    {
    public:
      size_t operator()(const File& file) const noexcept
      {
        //TODO: Make this absolute path
        return std::hash<std::string>()(file.Path());
      }
    };

    template<typename Func, typename...Args>
    Functional::Chain<std::invoke_result_t<Func, Args...>> DoIfNewer(const File& other, Func&& func, Args&&... args) const
    {
      return Functional::DoIf(other.Timestamp() < Timestamp(), std::forward<Func>(func), std::forward<Args>(args)...);
    }

    std::string ToString() const
    {
      return Path();
    }

    // Backs up the target file and then copies file refered to by this to dest
    void ArchiveAndReplace(const File& destFile);

    inline void EnsureSymlinkTo(const File& destFile) const
    {
      std::filesystem::remove(destFile.Path());
      std::filesystem::create_directories(destFile.Location().Path());
      std::filesystem::create_symlink(Path(), destFile.Path());
    }

    inline void EnsureCopyTo(const File& destFile) const
    {
      std::filesystem::remove(destFile.Path());
      std::filesystem::create_directories(destFile.Location().Path());
      std::filesystem::copy_file(Path(), destFile.Path());
    }

    bool operator==(const File& other) const
    {
      return Path() == other.Path();
    }

    static File CreateNew(const std::string_view& fileName, const std::string& defaultText = "");

    void Create(const std::string& defaultText = "") const;

    void Delete() const;

    template<typename Parser>
    auto ParseByLine(Parser&& parser) const
    {
      std::vector<std::invoke_result_t<Parser, std::string>> out{};
      std::ifstream file{this->Path()};
      std::string line{};
      while (std::getline(file, line))
      {
        out.push_back(parser(line));
      }

      return out;
    }

    std::vector<std::string> Parse() const
    {
      return this->ParseByLine([](const std::string& line) { return line; });
    }

  protected:
    void UpdatePath(const std::string_view& other)
    {
      m_fileInfo = std::filesystem::directory_entry{std::string{other}};
    }

  private:
    // This is mutable so that we can refresh the file cache from const contexts
    // the basic const gaurentee of this class is that the underlying file reference
    // will not change out from under you. Changing the filepath in a const context is
    // an error.
    mutable std::filesystem::directory_entry m_fileInfo{};
  };

}
