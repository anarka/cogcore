#pragma once
#include <unordered_set>
#include <string_view>
#include <CogCore/Logger.h>
#include <CogCore/File.h>
#include <CogCore/FunctionWrappers.h>
#include <type_traits>
#include <utility>

struct Null{};
using SplitString = std::pair<std::string_view, std::string_view>;

const auto ThrowHandler = [](auto&& input) { throw std::runtime_error("No delim found on line: " + std::string{input});  return SplitString{}; };

template<typename ArrayType, size_t Size>
size_t CArraySize(ArrayType(&)[Size])
{
  return Size;
}

template<typename ToSize>
size_t Size(ToSize&& in)
{
  if constexpr(std::is_same_v<char, std::decay_t<ToSize>>)
  {
    return 1;
  }
  else if constexpr(std::is_same_v<const char*, std::decay_t<ToSize>>)
  {
    return CArraySize(in);
  }
  else
  {
    return in.size();
  }
}

template<typename DelimType, typename ElseFunc = decltype(ThrowHandler), typename...Args>
inline SplitString Split(const std::string_view& input, DelimType&& delim, ElseFunc handlerFunc = ThrowHandler, Args&&...extraHandlerArgs)
{
  auto delimSize = Size(delim);

  auto delimPos = input.find(delim);

  if(delimPos != std::string::npos)
  {
    const auto key = input.substr(0, delimPos);
    const auto payload = input.substr(delimPos + delimSize);
    return std::pair{std::move(key), std::move(payload)};
  }
  else
  {
    return handlerFunc(input, std::forward<Args>(extraHandlerArgs)...);
  }
}

template<typename ElseFunc = decltype(ThrowHandler), typename...Args>
inline SplitString SplitFromEnd(const std::string_view& input, char delim, ElseFunc handlerFunc = ThrowHandler, Args&&...extraHandlerArgs)
{
  auto delimPos = input.find_last_of(delim);

  if(delimPos != std::string::npos)
  {
    const auto key = input.substr(0, delimPos);
    const auto payload = input.substr(delimPos + 1);
    return std::pair{std::move(key), std::move(payload)};
  }
  else
  {
    return handlerFunc(input, std::forward<Args>(extraHandlerArgs)...);
  }
}

template<typename DelimType, template<typename,typename...> typename Container = std::vector>
inline Container<std::string> Tokenize(const std::string_view& input, DelimType&& delim)
{
  Container<std::string> out{};
  std::string_view remaining{input};
  std::string_view token{};

  do
  {
    std::tie(token, remaining) = Split(remaining, delim, [](auto&& line) { return SplitString{line, ""}; });
    out.emplace_back(token);
  } while(!remaining.empty());

  return out;
}

#define GENERIC_ERASER(NAME) \
    template<typename RT, typename...Args>\
    class NAME##Eraser\
    {\
    public:\
      template<typename T>\
      NAME##Eraser(T& toTarget)\
        : m_target{&toTarget},\
          m_vtable{&vtable_for<std::decay_t<T>>}{}\
\
      RT NAME (Args&&... args)\
      {\
        return m_vtable-> NAME (m_target, std::forward<Args>(args)...);\
      }\
\
    private:\
      void* m_target;\
      struct vtable\
      {\
        RT (*NAME)(void* target, Args&&...);\
      };\
\
      template<typename T>\
      static inline vtable const vtable_for = \
      {\
        [](void* target, Args&&...args)\
        {\
          return static_cast<T*>(target)-> NAME (std::forward<Args>(args)...);\
        },\
      };\
\
      vtable const *const m_vtable;\
    };

UTILITY_DETECTOR(begin)

template<typename TargetType, typename CallFunc, typename...Args>
auto For(TargetType&& target, CallFunc&& func, Args&&...args)
{
  if constexpr(Hasbegin<TargetType>() && !std::is_same_v<std::string, std::decay_t<TargetType>> && !std::is_same_v<std::string_view, std::decay_t<TargetType>>)
  {
    for(auto&& val : target)
    {
      func(std::forward<decltype(val)>(val), std::forward<Args>(args)...);
    }
  }
  else
  {
    func(std::forward<TargetType>(target), std::forward<Args>(args)...);
  }
}


namespace Cog::Config
{
//  template<typename CogMetadataType = void>
  class ConfigFile : public Filesystem::File
  {
    Filesystem::File m_loadedFrom{};
  public:
    ConfigFile(const std::string_view& fileName)
      : Filesystem::File{fileName}
    {
    };

    ConfigFile(const Filesystem::File& configFile)
      : Filesystem::File{configFile}{};

    
    ConfigFile(const ConfigFile& other) = delete;
    ConfigFile(ConfigFile&& other) = delete;
    ConfigFile& operator=(const ConfigFile& other) = delete;
    ConfigFile& operator=(ConfigFile&& other) = delete;

    void Reload(const std::string_view& newFile)                    
    {                                                              
      UpdatePath(newFile);                                        
      Load();                                                    
    }

    const Filesystem::File& LoadedPath() const
    {
      return m_loadedFrom;
    }
  protected:
    template<typename Keymapable>
    void Register(Keymapable& toRegister)
    {
      m_setters.emplace(Keymapable::KEY, toRegister);
    }


    template<typename Finalizable>
    void Finalize(Finalizable& toFinalize)
    {
      m_finalizables.emplace(Finalizable::KEY, toFinalize);
    }

    void Load()
    {
      m_loadedFrom = *this;

      auto scopedLogger = Cog::Logging::ColorLog("Begining cog load %s", Cog::Logging::Colors::MAGENTA, Path());
      
      while (!m_loadedFrom.Exists())
      {
        if (m_loadedFrom.Ext() == "")
        {
          scopedLogger.Log("%s not found, no cog detected, default values will be used", m_loadedFrom);
          return;
        }
        
        scopedLogger.Log("Cog %s not present", m_loadedFrom);
        if (auto location = m_loadedFrom.Location().Path(); location != "")
        {
          m_loadedFrom = m_loadedFrom.LocatedAt(location + "/" + m_loadedFrom.Ext() + "/").WithExt("");
        }
        else
        {
          m_loadedFrom  = m_loadedFrom.LocatedAt("./" + m_loadedFrom.Ext() + "/").WithExt("");
        }

        scopedLogger.Log("Searching for %s", m_loadedFrom);
      }

      std::ifstream configFile{m_loadedFrom.Path()};
      std::string line;
   

      while(std::getline(configFile, line))
      {
        if(line.empty()) continue;

        try
        {
          auto [key, payload] = Split(line, '=');
          // In theory payload should always be null-terminated, but doing it the view way anyway since it is one
          scopedLogger.Log("Mapping var at key %.*s to %.*s", key.size(), key, payload.size(), payload);
          
          if(m_setters.count(key))
          {
            m_setters.at(key).Set(payload);
          }
          else
          {
            throw std::runtime_error("Key not recognized: " + std::string{key});
          }
        }
        catch(std::exception& e)
        {
          //Cog::Logging::ErrorLog("Failed to map line: %s", e.what());
        }
      }

      //for (auto& [key, functor] : m_finalizables)
      {
        //functor.Finalize();
      }
    }

  public:
    template<typename Metadata, typename Parent = ConfigFile>
    class Value
    {
      using ValueType = typename Metadata::ValueType;
      using RawValueType = std::remove_cvref_t<std::remove_pointer_t<ValueType>>;
    public:
      using MetadataType = Metadata;
      static constexpr auto KEY = Metadata::KEY;

      Value(Parent* parent)
       : m_parent{parent} 
      {
        if constexpr(!HasUnsettable<Metadata>())
        {
          parent->Register(*this);
        }

        if constexpr (HasFinalize<Metadata>())
        {
          parent->Finalize(*this);
        }
      };

      const RawValueType& operator()() const
      {
        if(!m_finalized)
        {
          const_cast<Value<Metadata, Parent>*>(this)->Finalize();
        }
        
        if constexpr(std::is_pointer_v<typename Metadata::ValueType>)
        {
          if (m_value == nullptr)
          {
            throw "Error: Cog finalized with a nullptr";
          }
          return *m_value;
        }
        else
        {
          return m_value;
        }
      }

      const RawValueType& operator()(std::string overrideValue) const 
      {
        static_assert(HasOverride<Metadata>(), "Error: Attempting to override non-overrideable cog value");
        const_cast<Value<Metadata, Parent>*>(this)->Set(overrideValue);
        return (*this)();
      }

      void Finalize()
      {
        if(!m_set)
        {
          if constexpr(HasDefaultValue<Metadata>())
          {
            m_value = Metadata::DefaultValue();
          }

          m_set = true;
        }
        
        if constexpr (HasFinalize<Metadata>())
        {
          if constexpr(HasFinalizeSignature<Metadata, ValueType&, Parent&>())
          {
            Metadata::Finalize(m_value, *m_parent);
          }
          else
          {
            Metadata::Finalize(m_value); 
          }
          
          m_finalized = true;
        }
      }

      void Set(std::string_view toSet)
      {
        m_set = true;

        if constexpr(HasKeyType<Metadata>())
        {
          MapSet(toSet);
        }
        else if constexpr(HasConvert<Metadata>())
        {
          For(MaybeTokenize<HasTokenizeConvertFlag<Metadata>()>(toSet), [this](auto&& process)
          {
            ConvertSet(process);
          });
        }
        else
        {
          if constexpr (HasAppend<Metadata>())
          {
            Append(typename Metadata::ValueType{toSet});
          }
          else
          {
            if constexpr(std::is_same_v<char, RawValueType>)
            {
              m_value = toSet[0];
            }
            else
            {
              m_value = RawValueType{toSet};
            }
          }
        }
        
        if constexpr(HasFinalize<Metadata>())
        {
          if(m_finalized)
          {
            Finalize();
          }
        }
      }

      template<typename KeyType, typename ValueType>
      auto Map(KeyType&& key, ValueType&& value)
      {
        if constexpr (HasMap<Metadata>())
        {
          if constexpr (HasMapSignature<Metadata, ValueType&, typename Metadata::KeyType&&, typename Metadata::MappedType&&, Parent&>())
          {
            return Metadata::Map(m_value, key, value, *m_parent);
          }
          else
          {
            //TODO: FIX THIS SHIT
            return Metadata::Map(m_value, std::move(key), std::move(value), *m_parent);
          }
        }
        else
        {
          return m_value.emplace(std::piecewise_construct, std::forward_as_tuple(key), std::forward_as_tuple(value));
        }
      }

      void MapSet(const std::string_view& inLine)
      {
        if constexpr (HasMappedType<Metadata>())
        {
          auto keyValuePairs = Tokenize(inLine, ';');
          for(auto&& pair : keyValuePairs)
          {
            auto [keyString, valueString] = Split(pair, ':', [](auto&& input) { return SplitString{input, {}}; } );

            For(MaybeTokenize<HasTokenizeKeyFlag<Metadata>()>(keyString), [valueString=valueString,this](auto&& key)
            {
              For(MaybeTokenize<HasTokenizeValueFlag<Metadata>()>(valueString), [this, &key](auto&& value)
              {
                ValidateAndMap(key, value);
              }); 
            });
          }
        }
        else
        {
          For(Tokenize(inLine, ','), [this](auto&& key)
          {
            m_value.emplace(ConvertKey(key));
          });
        }
      }

      template<bool ShouldTokenize>
      auto MaybeTokenize(std::string_view inLine)
      {
        if constexpr(ShouldTokenize)
        {
          return Tokenize(inLine, ',');
        }
        else
        {
          return inLine;
        }
      }

      void Append(const RawValueType& inVal)
      {
        if constexpr(HasAppendSignature<Metadata, std::string_view, Parent&>())
        {
          Metadata::Append(m_value, inVal, *m_parent);
        }
        else if constexpr(HasAppendSignature<Metadata, std::string_view, Parent&>())
        {
          Metadata::Append(m_value, inVal);
        }
        else
        {
          m_value = inVal;
        }
      }

      void ConvertSet(std::string_view inLine)
      {
        if constexpr (HasAppend<Metadata>())
        {
          if constexpr (HasConvertSignature<Metadata, std::string_view, Parent&>())
          {
            static_assert(std::is_same_v<std::invoke_result_t<decltype(Metadata::Convert),std::string_view, Parent&>, typename Metadata::ValueType>, 
                          "Error: Convert returns a different type than is exposed to Cog");
            Append(Metadata::Convert(inLine, *m_parent));
          }
          else
          {
            static_assert(std::is_same_v<std::invoke_result_t<decltype(Metadata::Convert),std::string_view>, typename Metadata::ValueType>, 
                          "Error: Convert returns a different type than is exposed to Cog");

            Append(Metadata::Convert(inLine));
          }
        }
        else
        {
          if constexpr (HasConvertSignature<Metadata, std::string_view, Parent&>())
          {
            static_assert(std::is_same_v<std::invoke_result_t<decltype(Metadata::Convert),std::string_view, Parent&>, typename Metadata::ValueType>, 
                          "Error: Convert returns a different type than is exposed to Cog");
            m_value = Metadata::Convert(inLine, *m_parent);
          }
          else
          {
            static_assert(std::is_same_v<std::invoke_result_t<decltype(Metadata::Convert),std::string_view>, typename Metadata::ValueType>, 
                          "Error: Convert returns a different type than is exposed to Cog");
            m_value = Metadata::Convert(inLine);
          }
        }
      }

      auto ConvertAndMap(std::string_view keyString, std::string_view valueString)
      {
        return Map(ConvertKey(keyString), ConvertValue(valueString));
      }

      auto ConvertValue(std::string_view valueString)
      {
        if constexpr(HasConvertValue<Metadata>())
        {
          if constexpr(HasConvertValueSignature<Metadata, std::string_view, Parent&>())
          {
            return Metadata::ConvertValue(valueString, *m_parent); 
          }
          else
          {
            return Metadata::ConvertValue(valueString);
          }
        }
        else
        {
          return typename Metadata::MappedType{valueString};
        }
      }

      auto ConvertKey(std::string_view valueString)
      {
        if constexpr(HasConvertKey<Metadata>())
        {
          if constexpr(HasConvertKeySignature<Metadata, std::string_view, Parent&>())
          {
            return Metadata::ConvertKey(valueString, *m_parent); 
          }
          else
          {
            return Metadata::ConvertKey(valueString);
          }
        }
        else
        {
          return typename Metadata::KeyType{valueString};
        }
      }

      template<typename KeyType, typename ValueType>
      auto ValidateAndMap(KeyType&& key, ValueType&& value)
      {
        if constexpr (HasUniqueMapFlag<Metadata>())
        {
          if (!m_value.contains(key))
          {
            return ConvertAndMap(key, value);
          }
          else
          {
            Logging::ErrorLog("Unique map variable %s already has key %s mapped", Metadata::KEY, key);
          }
        }
        else
        {
          return ConvertAndMap(key, value);
        }
      }

    private:
      typename Metadata::ValueType m_value{};
      Parent* m_parent{};
      bool m_finalized{false};
      bool m_set{false};
      UTILITY_DETECTOR(UniqueMapFlag)
      UTILITY_DETECTOR(KeyType)
      UTILITY_DETECTOR(MappedType)
      UTILITY_DETECTOR(TokenizeKeyFlag)
      UTILITY_DETECTOR(TokenizeValueFlag)
      UTILITY_DETECTOR(TokenizeConvertFlag)
      UTILITY_DETECTOR(ConvertKey)
      UTILITY_DETECTOR(ConvertValue)
      UTILITY_DETECTOR(Convert)
      UTILITY_DETECTOR(Finalize)
      UTILITY_DETECTOR(Append)
      UTILITY_DETECTOR(Map)
      UTILITY_DETECTOR(DefaultValue)
      UTILITY_DETECTOR(Override)
      UTILITY_DETECTOR(Unsettable)
    };

  private:
    GENERIC_ERASER(Set)
    std::unordered_map<std::string_view, SetEraser<void, const std::string_view&>> m_setters{};
    GENERIC_ERASER(Finalize)
    std::unordered_map<std::string_view, FinalizeEraser<void>> m_finalizables{}; 
  protected:
    void Register(std::string_view key, SetEraser<void, const std::string_view&> toSet)
    {
      m_setters.emplace(key, std::move(toSet));
    }
  };


  template<typename Key, typename Mapped, typename HashFunction = typename Key::Hashor>
  class StaticMappedObjects
  {
  public:
    static Mapped& Get(const Key& key)
    {
      return Get(key, key);
    }

    template<typename...MapConstructTypes>
    static Mapped& Get(const Key& key, MapConstructTypes&&...args)
    {
      auto& map = Map();
      if (auto keyPos = map.find(key); keyPos == map.end())
      {
        return map.emplace(std::piecewise_construct, std::forward_as_tuple(key), std::forward_as_tuple(std::forward<MapConstructTypes>(args)...)).first->second;
      }
      else
      {
        return keyPos->second;
      }
    }

  private:
    static auto& Map()
    {
      static std::unordered_map<Key, Mapped, HashFunction> m_map{};
      return m_map;
    }
  };


#define MAYBE_VAR(VAR_NAME, VAR_TYPE)           \
  UTILITY_DETECTOR(VAR_NAME)                    \
  template<bool needsValue>                     \
  class VAR_NAME                                \
  {                                             \
  public:                                       \
    template<typename...Args>                   \
    VAR_NAME (Args&&...args)                    \
      : m_value{std::forward<Args>(args)...}    \
    {}                                          \
                                                \
    const VAR_TYPE & operator()()               \
    {                                           \
      return m_value;                           \
    }                                           \
                                                \
  private:                                      \
    VAR_TYPE m_value;                           \
  };                                            \
                                                \
  template<>                                    \
  class VAR_NAME <false>                        \
  {                                             \
  public:                                       \
    template<typename...Args>                   \
    VAR_NAME (Args&&...){}                      \
  };

  MAYBE_VAR(Namespace, std::string)

  template<typename ChildCog>
  class CogFile : Filesystem::File
  {
    auto GetCogName()
    {
    };
  public:
    UTILITY_DETECTOR(CogName)
    
    Namespace<HasNamespace<ChildCog>()> m_namespace{};


    static_assert(HasCogName<ChildCog>(), "Error: no cog name provided!");
    CogFile() : 
      Filesystem::File(ChildCog::CogName)
    {
    };

    CogFile(const Filesystem::File& remoteDest) : 
      Filesystem::File(remoteDest.Path() + ChildCog::CogName)
    {
      static_assert(!HasNamespace<ChildCog>(), "Error: Attempting to construct namespaced CogFile without a namespace");
    };

    CogFile(const std::string& inNamespace) : 
      Filesystem::File{inNamespace + ChildCog::CogName},
      m_namespace{inNamespace}
    {
      static_assert(HasNamespace<ChildCog>(), "Error: Attempting to namespace construct a non-namespaced cog");
    };

    CogFile(const std::string& inNamespace, const Filesystem::File& remoteFile) : 
      Filesystem::File{remoteFile.Path() + inNamespace + ChildCog::CogName},
      m_namespace{inNamespace}
    {
      static_assert(HasNamespace<ChildCog>(), "Error: Attempting to namespace construct a non-namespaced cog");
    };

    CogFile(const CogFile& other) = delete;
    CogFile(CogFile&& other) = delete;
    CogFile& operator=(const CogFile& other) = delete;
    CogFile& operator=(CogFile&& other) = delete;
  
    void Load()
    {
      m_loadedFrom = *this;

      auto scopedLogger = Cog::Logging::ColorLog("Begining cog load %s", Cog::Logging::Colors::MAGENTA, Path());
      
      while (!m_loadedFrom.Exists())
      {
        if (m_loadedFrom.Ext() == "")
        {
          scopedLogger.Log("%s not found, no cog detected, default values will be used", m_loadedFrom);
          return;
        }
        
        scopedLogger.Log("Cog %s not present", m_loadedFrom);
        if (auto location = m_loadedFrom.Location().Path(); location != "")
        {
          m_loadedFrom = m_loadedFrom.LocatedAt(location + "/" + m_loadedFrom.Ext() + "/").WithExt("");
        }
        else
        {
          m_loadedFrom  = m_loadedFrom.LocatedAt("./" + m_loadedFrom.Ext() + "/").WithExt("");
        }

        scopedLogger.Log("Searching for %s", m_loadedFrom);
      }

      std::ifstream configFile{m_loadedFrom.Path()};
      std::string line;
   

      while(configFile >> std::ws >> line >> std::ws)
      {
        try
        {
          auto [key, payload] = Split(line, '=');
          // In theory payload should always be null-terminated, but doing it the view way anyway since it is one
          scopedLogger.Log("Mapping var at key %.*s to %.*s", key.size(), key, payload.size(), payload);
          
          if(m_setters.count(key))
          {
            m_setters.at(key).Set(payload);
          }
          else
          {
            throw std::runtime_error("Key not recognized: " + std::string{key});
          }
        }
        catch(std::exception& e)
        {
          Cog::Logging::ErrorLog("Failed to map line: %s", e.what());
        }
      }

      //for (auto& [key, functor] : m_finalizables)
      {
        //functor.Finalize();
      }
    }
  
  private:
    GENERIC_ERASER(Set)
    std::unordered_map<std::string_view, SetEraser<void, const std::string_view&>> m_setters{};
    GENERIC_ERASER(Finalize)
    std::unordered_map<std::string_view, FinalizeEraser<void>> m_finalizables{}; 
    
    Filesystem::File m_loadedFrom{};
  };

  struct AutoCaller
  {
    template<typename Func, typename...Args>
    AutoCaller(Func&& func, Args&&...args)
    {
      Functional::Curry(func, std::forward<Args>(args)...);
    }
  };

  class Test 
  {
  public:
    template<typename...Args>
    Test(Args&&...args): 
      m_loadedVals{std::forward<Args>(args)...}
    {}

    using Namespace = void;


  private:
    friend struct AutoCaller;
    void Load()
    {
      m_loadedVals.Load();
    }

    struct FileLoadMetadata
    {
      inline static constexpr auto CogName = "Testaoeu.cog";
      using Namespace = void;
    };

    CogFile<FileLoadMetadata> m_loadedVals{"remoteFile"};
    AutoCaller autoLoad{&Test::Load, this};
  }; 

#ifndef CONFIG_MACROS
#define CONFIG_MACROS

#define COG_DEFAULT_GET(FILE_NAME)                                                          \
  static const FILE_NAME##Cog & Get(const Cog::Filesystem::File& path = Cog::Filesystem::File{"./"}) \
  {                                                                                         \
    return Cog::Config::StaticMappedObjects<Cog::Filesystem::File, FILE_NAME##Cog >::Get(path);       \
  }

#define SUBCOG_DEFAULT_GET(FILE_NAME) \
  private:\
    ParentType* m_parent;\
  public:\
    const ParentType& Parent() const    \
    {\
      return *m_parent;\
    }\
    static const FILE_NAME##Cog & Get(ParentType& parent, const Cog::Filesystem::File& path = Cog::Filesystem::File{"./"}) \
    {                                                                                         \
      return Cog::Config::StaticMappedObjects<Cog::Filesystem::File, FILE_NAME##Cog >::Get(path, parent, path);       \
    }

#define COG_NAMESPACE_GET(FILE_NAME)                                                          \
  private:\
    std::string m_namespace{}; \
  public:\
    const std::string& Namespace() const { return m_namespace; }     \
  static const FILE_NAME##Cog & Get(std::string cogNamespace, const Cog::Filesystem::File& path = Cog::Filesystem::File{"./"}) \
  {                                                                                         \
    return Cog::Config::StaticMappedObjects<Cog::Filesystem::File, FILE_NAME##Cog >::Get(path.WithExt(cogNamespace), cogNamespace, path);       \
  }

#define SUBCOG_NAMESPACE_GET(FILE_NAME) \
  private:\
    std::string m_namespace{}; \
  public:\
    const std::string& Namespace() const { return m_namespace; }     \
  private:\
    ParentType* m_parent;\
  public:\
    const ParentType& Parent() const    \
    {\
      return *m_parent;\
    }\
    static const FILE_NAME##Cog & Get(const std::string& cogNamespace, ParentType& parent, const Cog::Filesystem::File& path = Cog::Filesystem::File{"./"}) \
    {                                                                                         \
      return Cog::Config::StaticMappedObjects<Cog::Filesystem::File, FILE_NAME##Cog >::Get(path.WithExt(cogNamespace), parent, cogNamespace, path );       \
    }

#define BASIC_COG_FILE(FILE_NAME)                                        \
  class FILE_NAME##Cog : public Cog::Config::ConfigFile                    \
  {                                                                   \
  public:                                                             \
    using CurrentFile = FILE_NAME##Cog;                               

#define COG(FILE_NAME) \
    BASIC_COG_FILE(FILE_NAME) \
      FILE_NAME##Cog () : Cog::Config::ConfigFile(#FILE_NAME ".cog"){ Load();};                  \
                                                                      \
      FILE_NAME##Cog (const Cog::Filesystem::File& remoteFile)                    \
        : Cog::Config::ConfigFile{remoteFile.Path() + #FILE_NAME ".cog"}                                        \
      {                                                                 \
        if(Fullname() != #FILE_NAME ".cog")                  \
        {                                                               \
          throw std::runtime_error(std::string{"Target file has incorrect name "} + remoteFile.Fullname());   \
        }                                                               \
        Load();                                                         \
      };                                                                \
      COG_DEFAULT_GET(FILE_NAME)  

#define NAMESPACED_COG(FILE_NAME) \
    BASIC_COG_FILE(FILE_NAME)  \
      COG_NAMESPACE_GET(FILE_NAME)\
      FILE_NAME##Cog (std::string cogNamespace = "") \
        : Cog::Config::ConfigFile(cogNamespace + "." #FILE_NAME ".cog"), \
          m_namespace{std::move(cogNamespace)} \
        {           \
          Load(); \
        }; \
                                    \
      FILE_NAME##Cog (std::string cogNamespace, const Cog::Filesystem::File& remoteFile)                    \
        : Cog::Config::ConfigFile{remoteFile.Path() + cogNamespace + "." #FILE_NAME ".cog"},                                        \
          m_namespace{std::move(cogNamespace)} \
      {                                                                 \
        if(m_namespace == "" || Fullname() != m_namespace + "." #FILE_NAME ".cog")                  \
        {                                                               \
          throw std::runtime_error(std::string{"Target file has incorrect name "} + remoteFile.Fullname());   \
        }                                                               \
        Load();                                                         \
      };

#define SUBCOG(FILE_NAME) \
    template<typename ParentType>\
    BASIC_COG_FILE(FILE_NAME) \
      SUBCOG_DEFAULT_GET(FILE_NAME) \
      FILE_NAME##Cog (ParentType& parent) \
        : Cog::Config::ConfigFile{#FILE_NAME ".cog"}, \
          m_parent{&parent}\
      {\
        Load();\
      } \
      FILE_NAME##Cog (ParentType& parent, const Cog::Filesystem::File& remoteFile)                    \
        : Cog::Config::ConfigFile{remoteFile.Path() + #FILE_NAME ".cog"},                                        \
          m_parent{&parent}\
      {                                                                 \
        if(Fullname() != #FILE_NAME ".cog")                  \
        {                                                               \
          throw std::runtime_error(std::string{"Target file has incorrect name "} + remoteFile.Fullname());   \
        }                                                               \
        Load();                                                         \
      }; 

#define NAMESPACED_SUBCOG(FILE_NAME) \
    template<typename ParentType>\
    BASIC_COG_FILE(FILE_NAME) \
      SUBCOG_NAMESPACE_GET(FILE_NAME) \
      FILE_NAME##Cog (ParentType& parent, std::string cogNamespace = "") \
        : Cog::Config::ConfigFile{cogNamespace + "." #FILE_NAME ".cog"}, \
          m_namespace{cogNamespace},\
          m_parent{&parent}\
      {\
        Load();\
      } \
      FILE_NAME##Cog (ParentType& parent, std::string cogNamespace, const Cog::Filesystem::File& remoteFile)                    \
        : Cog::Config::ConfigFile{remoteFile.Path() + cogNamespace + "." #FILE_NAME ".cog"},                                        \
          m_namespace{std::move(cogNamespace)}, \
          m_parent{&parent}\
      {                                                                 \
        if(m_namespace == "" || Fullname() != m_namespace + "." #FILE_NAME ".cog")                  \
        {                                                               \
          throw std::runtime_error(std::string{"Target file has incorrect name "} + remoteFile.Fullname());   \
        }                                                               \
        Load();                                                         \
      };

#define EXTENTION_COG(FILE_NAME)

#define END_COG() \
  };

#define COG_DEFAULT_VALUE(DEFAULT_VALUE) \
  static decltype(DEFAULT_VALUE)& DefaultValue() \
  {\
    static auto&& defaultVal = DEFAULT_VALUE;\
    return defaultVal;\
  }

#define COG_MAGIC_VALUE(KEY, DEFAULT_VALUE)         \
  private:                                          \
    using KEY##Type = decltype(DEFAULT_VALUE);      \
    const KEY##Type m_##KEY = DEFAULT_VALUE;        \
  public:                                           \
    const KEY##Type& KEY () const                   \
    {                                               \
      return m_##KEY;                               \
    }

#define BASIC_COG_VARIABLE(NAME, TYPE) \
  private:                                                                        \
    struct NAME##Metadata                                                      \
    {                                                                             \
      using ValueType = TYPE;                                  \
      static constexpr std::string_view KEY = #NAME "\0";                      

#define NEW_COG_VARIABLE(NAME, DEFAULT_VALUE)                                  \
      BASIC_COG_VARIABLE(NAME, decltype(DEFAULT_VALUE))        \
      COG_DEFAULT_VALUE(DEFAULT_VALUE)                           

#define NEW_COG_SET(NAME, KEY_TYPE) \
      BASIC_COG_VARIABLE(NAME, std::unordered_set<KEY_TYPE>) \
      using KeyType = KEY_TYPE;

#define NEW_COG_MAP(NAME, KEY_TYPE, MAPPED_TYPE)                            \
      NEW_COG_VARIABLE(NAME, (std::unordered_map<KEY_TYPE, MAPPED_TYPE>{})) \
      using KeyType = KEY_TYPE;                                             \
      using MappedType = MAPPED_TYPE;                                     

#define END_COG_VARIABLE(KEY)                                               \
    };                                                                      \
  public:                                                                   \
    Cog::Config::ConfigFile::Value< KEY##Metadata, CurrentFile> KEY {this};

#define END_PRIVATE_COG_VARIABLE(KEY)                                       \
    };                                                                      \
    Cog::Config::ConfigFile::Value< KEY##Metadata, CurrentFile> KEY {this};

#define CONVERT_FUNCTION()                    \
  static auto Convert(std::string_view inVal) 

#define MY_CONVERT_FUNCTION() \
  static auto Convert(std::string_view inVal, CurrentFile& My)

#define KEY_CONVERT_FUNCTION()                      \
  static auto ConvertKey(std::string_view toConvert)

#define MY_KEY_CONVERT_FUNCTION() \
  static auto ConvertKey(std::string_view toConvert, CurrentFile& My)

#define VALUE_CONVERT_FUNCTION() \
  static auto ConvertValue(std::string_view toConvert)

#define MY_VALUE_CONVERT_FUNCTION() \
  static auto ConvertValue(std::string_view toConvert, CurrentFile& My)

#define FINALIZE_FUNCTION() \
  static auto Finalize(std::remove_cv_t<ValueType>& toFinalize)

#define MY_FINALIZE_FUNCTION() \
  static auto Finalize(std::remove_cv_t<ValueType>& toFinalize, CurrentFile& My)

#define MY_APPEND_FUNCTION() \
  static auto Append(std::remove_cv_t<ValueType>& toAppend, ValueType&& inVal, CurrentFile& My)

#define APPEND_FUNCTION() \
  static auto Append(std::remove_cv_t<ValueType>& toAppend, ValueType&& inVal)

#define MAP_FUNCTION() \
  static auto Map(ValueType& map, const KeyType& key, const MappedType& value)

#define MY_MAP_FUNCTION() \
  static auto Map(ValueType& map, KeyType&& key, MappedType&& value, CurrentFile& My)

#define NEW_COG_SIMPLE_VAR(NAME, DEFAULT_VALUE) \
  NEW_COG_VARIABLE(NAME, DEFAULT_VALUE) \
  END_COG_VARIABLE(NAME)

#define NEW_COG_SIMPLE(NAME, DEFAULT_VALUE)                \
  NEW_COG_SIMPLE_VAR(NAME, std::string{ DEFAULT_VALUE })   

#define NEW_COG_SIMPLE_SET(NAME, KEY_TYPE) \
  NEW_COG_SET(NAME, KEY_TYPE) \
  END_COG_VARIABLE(NAME)

#define TOKENIZE_VALUES using TokenizeValueFlag = void;

#define TOKENIZE_KEYS using TokenizeKeyFlag = void; 

#define TOKENIZE_CONVERT using TokenizeKeyFlag = void;

#define UNIQUE_MAPPING using UniqueMapFlag = void;

#define OVERRIDEABLE using Override = void;

#define NEW_COG_DERIVED(KEY, DEFAULT_VALUE)                                                                       \
  BASIC_COG_VARIABLE(KEY, DEFAULT_VALUE)                                                                          \
    using Unsettable = void;              \
    MY_FINALIZE_FUNCTION()

#define DEFAULT_DERIVED(KEY)        \
  DERIVED_VALUE(KEY, std::string{}) 

#endif
}
