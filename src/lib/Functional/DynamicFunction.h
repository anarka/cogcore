#pragma once

#include <functional>
#include <string>
#include <dlfcn.h> 
#include <stdexcept>
#include <CogCore/File.h>
#include <CogCore/Variables.h>

namespace Cog::Functional
{
  template<typename Target>
  std::remove_cv_t<Target>& Mutable(Target&& target)
  {
    return const_cast<std::remove_cv_t<Target>&>(target);
  }

  template<typename Forwarded>
  auto&& Forward(Forwarded&& forward)
  {
    return std::forward<Forwarded>(forward);
  }

  class DynamicLib
  {
  };

  template<typename DynamicLibrary, typename FunctionSignature>
  class DynamicFunction
  {
  public:
    
    DynamicFunction(const DynamicLibrary& loadFrom, std::string func) :
      myFunctionLib{loadFrom},
      myFunctionName{std::move(func)}
    {
    }

    template<typename...Args>
    auto operator()(Args&&...args) const
    {
      if (myLoadedFunction == nullptr)
      {
        const_cast<DynamicFunction<DynamicLibrary, FunctionSignature>*>(this)->myLoadedFunction = reinterpret_cast<FunctionSignature>(myFunctionLib.Load(myFunctionName));
      
        if (myLoadedFunction == nullptr)
        {
          throw std::runtime_error("Function failed to load!");
        }
      }

      return (myLoadedFunction)(Forward(args)...);
    }

  private:
    const DynamicLibrary& myFunctionLib;
    std::string myFunctionName{};
    FunctionSignature myLoadedFunction{};
  };

  class SharedObject
  {
  public:
    SharedObject(std::string libName) :
      myLibraryName{std::move(libName)},
      myLibraryHandle{dlopen(myLibraryName.c_str(), RTLD_NOW|RTLD_GLOBAL)}
    {
      if(myLibraryHandle == nullptr)
      {
        throw std::runtime_error("Failed to load library " + myLibraryName + " with error " + dlerror());
      }
    }

    ~SharedObject()
    {
      dlclose(myLibraryHandle);
    }

    template<typename FunctionSignature>
    DynamicFunction<SharedObject, FunctionSignature> GetFunction(const std::string& toLoad) const
    {
      return DynamicFunction<SharedObject, FunctionSignature>{*this, toLoad};
    } 

    void* Load(const std::string& functionName) const
    {
      auto* out = dlsym(myLibraryHandle, functionName.c_str());
      if(out == nullptr)
      {
        throw std::runtime_error("Failed to load function " + functionName + " with error " + dlerror());
      }

      return out;
    }

  private:
    std::string myLibraryName{};
    void* myLibraryHandle;
  };

  template<typename Builder>
  class JITObject : public Filesystem::File
  {
  public:
    JITObject(const Filesystem::File& filePath, const Filesystem::File& targetDir) :
      Filesystem::File{filePath},
      myTargetObject{Builder::Build(filePath, targetDir).Path()}
    {}

    SharedObject myTargetObject;
  };
}
