#pragma once
#include <functional>
#include <cstdio>

#define UTILITY_DETECTOR(UTILITY_NAME)                                                                                                                                                                                                        \
  struct UTILITY_NAME##Detect\
  {\
    template<typename DetectType> \
    static constexpr decltype(DetectType::UTILITY_NAME , true) Has##UTILITY_NAME (int) \
    {\
      return true;\
    }\
    template<typename>\
    static constexpr bool Has##UTILITY_NAME (...)\
    {\
      return false;\
    }\
    template<typename DetectType> \
    static constexpr decltype(std::declval<typename DetectType::UTILITY_NAME>() , true) HasTypename (int) \
    {\
      return true;\
    }\
    template<typename>\
    static constexpr bool HasTypename (...)\
    {\
      return false;\
    }\
    template<typename DetectType, typename...Args> \
    static constexpr decltype(std::declval<DetectType>(). UTILITY_NAME (std::declval<Args>()...) , true) Has##UTILITY_NAMEFunc (int) \
    {\
      return true;\
    }\
    template<typename,typename...>\
    static constexpr bool Has##UTILITY_NAMEFunc (...)\
    {\
      return false;\
    }\
  };\
  template<typename DetectType, typename...Args>\
  static constexpr bool Has##UTILITY_NAME () \
  { \
    return UTILITY_NAME##Detect::template Has##UTILITY_NAME <std::decay_t<DetectType>>(0) || UTILITY_NAME##Detect::template Has##UTILITY_NAMEFunc <std::decay_t<DetectType>, Args...>(0) || UTILITY_NAME##Detect::template HasTypename<DetectType>(0); \
  } \
  template<typename DetectType, typename...Args>\
  static constexpr bool Has##UTILITY_NAME##Signature () \
  { \
    return UTILITY_NAME##Detect::template Has##UTILITY_NAMEFunc <std::decay_t<DetectType>, Args...>(0); \
  }

namespace Cog::Functional
{
  UTILITY_DETECTOR(Default)
  UTILITY_DETECTOR(ValueType)

  template<typename Head, typename...Tail>
  struct TypeList
  {
    using HeadType = Head;
    using TailType = TypeList<Tail...>;
    constexpr static size_t SIZE = 1 + TailType::SIZE;

    template<typename...Args>
    static constexpr bool Complete()
    {
      return (TypeList<Args...>::template Contains<HeadType>() || HasDefault<HeadType>()) && TailType::template Complete<Args...>(); 
    }

    template<typename CheckType>
    static constexpr bool Contains()
    {
      if constexpr(std::is_same_v<HeadType, CheckType>)
      {
        return true;
      }
      else
      {
        return TailType::template Contains<CheckType>();
      }
    }

    template<typename CheckType, size_t ListSize = SIZE>
    static consteval size_t Index()
    {
      if constexpr(std::is_same_v<HeadType, CheckType>)
      {
        return ListSize - SIZE;
      }
      else
      {
        return TailType::template Index<CheckType, ListSize>();
      }
    }

    template<size_t Index, size_t ListSize = SIZE>
    static constexpr auto TypeAt()
    {
      static_assert(Index <= SIZE, "Error: Index requested out of bounds!");
      if constexpr(Index == ListSize - SIZE)
      {
        return HeadType{};
      }
      else
      {
        return TailType::template TypeAt<Index, ListSize>();
      }
    }
  };

  template<typename Head>
  struct TypeList<Head>
  {
    constexpr static size_t SIZE = 1;
    using HeadType = Head;
   
    template<typename...Args>
    static constexpr bool Complete()
    {
      return (TypeList<Args...>::template Contains<HeadType>() || HasDefault<HeadType>()); 
    }

    template<typename CheckType>
    static constexpr bool Contains()
    {
      if constexpr(std::is_same_v<HeadType, CheckType>)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    
    template<typename CheckType, size_t ListSize = SIZE>
    static consteval size_t Index()
    {
      if constexpr(std::is_same_v<HeadType, CheckType>)
      {
        return ListSize - SIZE;
      }
      else
      {
        return std::numeric_limits<size_t>::max();
      }
    }
    
    template<size_t Index, size_t SetSize = SIZE>
    static constexpr auto TypeAt()
    {
      static_assert(Index <= SetSize, "Error: Index requested out of bounds!");
      if constexpr(Index == SetSize - SIZE)
      {
        return HeadType{};
      }
      else
      {
        return;
      }
    }
  };

  template<typename CheckType, typename...Args>
  static constexpr bool NoVoid()
  {
    if constexpr(sizeof...(Args) == 0)
    {
      return !std::is_same_v<void, CheckType>;
    }
    else
    {
      return !std::is_same_v<void, CheckType> && NoVoid<Args...>();
    }
  }

  template<typename Head, typename...Tail>
  struct ArgumentList : TypeList<Head, Tail...>
  {
    static_assert(NoVoid<Head, Tail...>(), "Cannot have a void argument!");
  };

  template<typename FirstArg, typename...Args>
  auto PeelFirst(FirstArg&& first, Args&&...)
  {
    return std::forward<FirstArg>(first);
  }

  
  template<size_t Position, size_t Current = 0, typename...Args> 
  auto ValueAtPosition(Args&&...args)
  {
    static_assert((Current <= Position && Position <= sizeof...(Args) + 1), "Error: Requesting value outsize the bounds of the parameter pack");
    if constexpr(Current == Position)
    {
      return PeelFirst(std::forward<Args>(args)...);
    }
    else
    {
      return ValueAtPosition<Position, Current + 1>(std::forward<Args>(args)...);
    }
  }
  
  template<typename TargetType, size_t Position, size_t Current = 0, typename...Args>
  auto ValueAtOrDefault(Args&&...args)
  {
    static_assert((Current <= Position && Position <= sizeof...(Args) + 1) || Position == std::numeric_limits<size_t>::max(), "Error: Requesting value outsize the bounds of the parameter pack");
    if constexpr(Position == std::numeric_limits<size_t>::max())
    {
      static auto eat = [](auto&&...){};
      eat(args...);
      static_assert(HasDefault<TargetType>(), "Error: Type without default value not assigned a value in function call");
      return TargetType::Default;
    }
    else
    {
      return ValueAtPosition<Position, Current>(args...);
    }
  }

  template<typename RT, typename...Args>
  struct FuncStruct 
  {  
    using ReturnType = RT;
    using ArgTypes = decltype(ArgumentList<Args...>{});
    static constexpr auto Arity = sizeof...(Args);

    template<typename Type>
    static auto InterfaceType()
    {
      if constexpr(HasValueType<Type>())
      {
        return typename Type::ValueType{};
      }
      else
      {
        return Type{};
      }
    }

    template<typename...CallArgs, size_t...NumArgs>
    static auto CallImpl(std::index_sequence<NumArgs...>, CallArgs&&...args)
    {
      return Function(ValueAtOrDefault<decltype(ArgTypes::template TypeAt<NumArgs>()), ArgumentList<CallArgs...>::template Index<decltype(ArgTypes::template TypeAt<NumArgs>())>(), 0, CallArgs...>(std::forward<CallArgs>(args)...)...);
    }

    template<typename...CallArgs>
    static auto Call(CallArgs&&...args)
    {
      static_assert(sizeof...(args) <= ArgTypes::SIZE, "Error: Too many arguments passed to function call!");
      static_assert((... && ArgTypes::template Contains<CallArgs>()), "Error: Attempting to call function with arguments of incorrect type");
      static_assert(ArgTypes::template Complete<CallArgs...>(), "Error: Arguments passed in are not sufficient to call the function with");

      return CallImpl(std::index_sequence_for<Args...>{}, std::forward<CallArgs>(args)...); 
    }

    static auto Function(decltype(InterfaceType<Args>())...)
    {
      printf("Yay");
      return true;
    }
  };

  struct Double
  {
    using ValueType = double;
    static constexpr ValueType Default = 0.0;
  };

  //static inline bool Bullshit = FuncStruct<bool, Double, float, int>::Call(1, 0.f);

  struct OtherFuncStruct 
  {
    template<typename RT, typename...Args>
    struct FunctionSignature
    {
      using ReturnType = RT;
      using ArgTypes = decltype(ArgumentList<Args...>{});
      static constexpr auto Arity = sizeof...(Args);
    };

    //static_assert(std::is_same_v<Overload<>::ReturnType, void>);
  };
  
  template<typename T>
  auto Identor(const T& t)
  {
    return [=]() { return t; };
  }

  template<typename T>
  auto Identor()
  {
    return [](auto&& thing) { return thing; };
  }
  
  template<typename Func, typename This, typename...Args>
  auto MemberCall(Func&& function, This&& target, Args&&...args)
  {
    if constexpr (std::is_pointer_v<std::decay_t<This>>)
    {
      return (*target.*function)(std::forward<Args>(args)...);
    }
    else
    {
      return (target.*function)(std::forward<Args>(args)...);
    }
  }

  template<typename Func, typename...Args>
  auto Call(Func&& function, Args&&...args)
  {
    if constexpr(std::is_member_function_pointer_v<std::decay_t<Func>>)
    {
      return MemberCall(std::forward<Func>(function), std::forward<Args>(args)...);
    }
    else
    {
      return function(std::forward<Args>(args)...);
    }
  }

  template<typename Func>
  auto MaybeCall(Func&& func)
  {
    if constexpr(std::is_invocable_v<Func>)
    {
      return func();
    }
    else
    {
      return func;
    }
  }

  template<typename Func, typename...Args>
  auto PartialApply(Func&& function, Args&&...args)
  {
    return [&function, &args...](auto&&...maybeMore)
    {
        if constexpr(std::is_invocable_v<Func, Args..., decltype(maybeMore)...>)
        {
            return Call(std::forward<Func>(function), std::forward<Args>(args)..., std::forward<decltype(maybeMore)>(maybeMore)...);
        }
        else
        {
            return PartialApply(std::forward<Func>(function), std::forward<Args>(args)..., std::forward<decltype(maybeMore)>(maybeMore)...);
        }
    };
  }

  template<typename Func, typename Curried, typename...Args>
  auto Ladel(Func&& function, Curried&& curried, Args&&...args);

  template<typename Func, typename...Args>
  auto CurryRecurse(Func&& function, Args&&...args)
  {
    if constexpr(sizeof...(Args) == 0)
    {
        return MaybeCall(std::forward<Func>(function));
    }
    else
    {
        return Ladel(std::forward<Func>(function), std::forward<Args>(args)...);
    }
  }

  template<typename Func, typename...Args>
  auto Curry(Func&& function, Args&&...args)
  {
    auto PartialApplyer = [](auto&&...why)
    {
        return PartialApply(std::forward<decltype(why)>(why)...);
    };
    
    return CurryRecurse(PartialApplyer, std::forward<Func>(function), std::forward<Args>(args)...);
  }

// vodo code from https://stackoverflow.com/questions/16337610/how-to-know-if-a-type-is-a-specialization-of-stdvector
template<typename Test, template<typename...> class Ref>
struct is_specialization : std::false_type {};

template<template<typename...> class Ref, typename... Args>
struct is_specialization<Ref<Args...>, Ref>: std::true_type {};

//template<template<typename...> class Ref, typename... Args>
//using is_speciazlization_v = is_specialization<Ref, Args...>::value;

  template<typename Func, typename Tuple, size_t...I, typename...Args>
  auto Unpack(Func&& function, Tuple&& tuple, std::index_sequence<I...>, Args&&...args)
  {
    return Call(std::forward<Func>(function), std::forward<Args>(args)..., std::get<I>(tuple)...);
  }

  template<typename Func, typename Curried, typename...Args>
  auto Ladel(Func&& function, Curried&& curried, Args&&...args)
  {
    if constexpr(is_specialization<std::decay_t<Curried>, std::tuple>::value)
    {
        static constexpr auto TUPLE_SIZE = std::tuple_size<std::decay_t<Curried>>::value;
        if constexpr(sizeof...(Args) == 0)
        {
          return MaybeCall(Unpack(std::forward<Func>(function), std::forward<Curried>(curried), std::make_index_sequence<TUPLE_SIZE>{}));
        }
        else
        {
          return CurryRecurse(Unpack(std::forward<Func>(function), std::forward<Curried>(curried), std::make_index_sequence<TUPLE_SIZE>{}), std::forward<Args>(args)...);
        }
    }
    else if constexpr(std::is_same_v<std::invoke_result_t<decltype(Call<Func, Curried>), Func, Curried>, void>)
    {
      // TODO: Allow extra args to pass through?
      static_assert(sizeof...(Args) == 0, "Error, extra arguments passed to function");
      Call(std::forward<Func>(function), std::forward<Curried>(curried));
    }
    else
    {
      return CurryRecurse(Call(std::forward<Func>(function), std::forward<Curried>(curried)), std::forward<Args>(args)...);
    }
  }

  template<typename Func, typename This, typename...Args>
  auto OnThis(Func&& memberFunc, This&& callOn, Args&&...args)
  {
    return Curry(std::forward<Func>(memberFunc), std::forward<This>(callOn), std::forward<Args>(args)...);
  }
  
  template<typename Func, typename...Args>
  constexpr bool IsCallable()
  {
    return std::is_invocable_v<Func, Args...>;
  }

  template<typename Func, typename...Args>
  constexpr auto Wrap(Func&& function, Args&&...args)
  {
    if constexpr (IsCallable<Func>() && sizeof...(args) == 0)
    {
      return function;
    }
    else
    {
      return [&function, &args...]() { return Curry(std::forward<Func>(function), std::forward<Args>(args)...); };
    }
  }

  template<typename Func, typename...Args>
  auto Passthrough(Func&& function, Args&&...args)
  {
    return [&function, &args...](auto&& passthroughArg)
    {
      static_assert(std::is_invocable_v<decltype(Curry<Func, decltype(passthroughArg), Args...>), Func, decltype(passthroughArg), Args...>, "Error: Passthrough on function not invocable");

      if constexpr (std::is_same_v<std::invoke_result_t<decltype(Curry<Func, decltype(passthroughArg), Args...>), Func, decltype(passthroughArg), Args...>, void>)
      {
        Curry(std::forward<Func>(function), std::forward<decltype(passthroughArg)>(passthroughArg), std::forward<Args>(args)...);
        return passthroughArg;
      }
      else
      {
        return std::make_tuple(std::forward<decltype(passthroughArg)>(passthroughArg), Curry(std::forward<Func>(function), std::forward<decltype(passthroughArg)>(passthroughArg), std::forward<Args>(args)...));
      }
    };
  }

  template<typename Func, typename...Args>
  auto AlphaAndOmega(Func&& function, Args&&...args)
  {
    return [&function, &args...](auto&&... toDup)
    {
      return MaybeCall(Curry(std::forward<Func>(function), toDup..., std::forward<Args>(args)..., toDup...));
    };
  }

  template<typename Pred>
  constexpr bool Predicate(Pred&& predicate)
  {
    if constexpr(std::is_convertible_v<std::decay_t<Pred>, bool>)
    {
      return predicate;
    }
    else if constexpr(std::is_convertible_v<decltype(std::declval<std::decay_t<Pred>>()()), bool>)
    {
      return predicate();
    }
  }

  template<typename Pred, typename Func, typename...Args>
  auto FunctionIf(Pred&& predicate, Func&& function, Args&&...args)
  {
    using ReturnType = std::invoke_result_t<Func, Args...>;
    using OutputType = std::optional<std::function<ReturnType()>>;

    if(Predicate(std::forward<Pred>(predicate)))
    {
      return OutputType{Wrap(std::forward<Func>(function), std::forward<Args>(args)...)};
    }
    else
    {
      return OutputType{std::nullopt};
    }
  }

  template<typename Return = void>
  class Chain
  {
  public:
    template<typename Pred, typename Func, typename...Args>
    Chain(Pred&& run, Func&& function, Args&&...args)
      : m_maybeFunction{FunctionIf(std::forward<Pred>(run), std::forward<Func>(function), std::forward<Args>(args)...)}{}

    ~Chain() noexcept(false)
    {
      (*this)();
    }


    Return operator()()
    {
      auto toRun = m_maybeFunction.value_or([](){});

      m_maybeFunction.reset();
      
      return toRun();
    };

    template<typename Pred, typename Func, typename...Args>
    Chain& ElseIf(Pred&& pred, Func&& function, Args&&...args)
    { 
      m_maybeFunction.emplace(m_maybeFunction.value_or(*FunctionIf(pred, std::forward<Func>(function), std::forward<Args>(args)...)));
      return *this;
    }    

    template<typename Func, typename...Args>
    Chain& Else(Func&& func, Args&&... args)
    {
      m_maybeFunction.emplace(m_maybeFunction.value_or(Wrap(std::forward<Func>(func), std::forward<Args>(args)...)));
      return *this;
    }

  private:
    // TODO: Ech, make this something better than the heavyweight std::function
    std::optional<std::function<Return()>> m_maybeFunction{};
  };

  template<typename Pred, typename Func, typename...Args>
  auto DoIf(Pred&& predicate, Func&& function, Args&&...args)
  {
    return Chain{std::forward<Pred>(predicate), std::forward<Func>(function), std::forward<Args>(args)...};
  }
}
