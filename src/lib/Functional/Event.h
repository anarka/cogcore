#pragma once
#include <vector>
#include <functional>

namespace Cog::Functional
{
  template<typename Key, typename Value>
  class HashTable
  {
  public:

  private:
    std::vector<std::pair<size_t, size_t>> myHashes{};
    std::vector<Value> myValues{};
  };

  template<typename ReturnType, typename...Args>
  class Event
  {
  public:
     
  private:
    struct EventHandle
    {
    public:

    private:
      size_t myHandle;
    };

    using HashType = size_t;
    using OffsetType = size_t;
    std::vector<std::pair<HashType, OffsetType>> myHandles{};
    std::vector<std::pair<std::function<ReturnType(Args...)>, OffsetType>> myCallbacks{};
  };
};
