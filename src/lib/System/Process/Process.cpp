#include <CogCore/Logger.h>
#include <stdexcept>
#include "Process.h"

namespace Cog::System
{
    bool Process::IsChild() const
    {
      return m_pid == 0;
    }

    void Process::EnsureNotChild() const
    {
      if(IsChild())
      {
        throw std::runtime_error("Calling parent only function from child");
      }
    }

    void Process::UpdateStats()
    {
      waitpid(m_pid, &m_stats, WNOHANG);
    }

    Process::~Process()
    {
      if(!IsExited())
      {
        Logging::ErrorLog("Killing child process %s that would have been abandoned!", m_program);
        kill(m_pid, SIGKILL);
      }
    }

    bool Process::IsExited()
    {
      EnsureNotChild();
      bool childExited = WIFEXITED(m_stats);
      if(!childExited)
      {
        UpdateStats();
        childExited = WIFEXITED(m_stats);
      }

      return childExited;
    }

    int Process::ReturnValue()
    {
      EnsureNotChild();

      waitpid(m_pid, &m_stats, 0);

      if(WIFEXITED(m_stats))
      {
        return WEXITSTATUS(m_stats);
      }
      else
      {
        throw std::runtime_error("No return val");
      }
    }

    void Process::Fork()
    {
      m_pid = fork();

      if(m_pid == -1)
      {
        throw std::runtime_error("Fork failed!");
      }

      if(IsChild())
      {
        m_stdOut.WriteTo(STDOUT_FILENO);
        m_stdErr.WriteTo(STDERR_FILENO);
        m_stdOut.CloseAll();
        m_stdErr.CloseAll();
      }
    }

    Process::Process(const std::string& program, const std::vector<std::string>& args) :
      m_program{program}
    {
      Fork();

      if(IsChild())
      {
        auto compat = new const char*[args.size() + 2];

        compat[0] = m_program.c_str();

        for(size_t i = 0; i < args.size(); ++i)
        {
          compat[i + 1] = args[i].c_str();
        }

        compat[args.size() + 1] = NULL;

        execvp(m_program.c_str(), (char**)compat);
        exit(125);
      }
    }
}
