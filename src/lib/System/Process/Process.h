#pragma once


#include <functional>
#include <type_traits>

#include <unistd.h>
#include <sys/wait.h>

#include <CogCore/Logger.h>
#include <CogCore/Fork.h>
#include <CogCore/Pipe.h>
#include <CogCore/Vector.h>

namespace Cog::System
{
  template<typename T>
  constexpr auto GetString(T&& val)
  {
    if constexpr(std::is_same_v<std::decay_t<T>, char const*>)
    {
      return val;
    }
    else
    {
      return val.c_str();
    }
  }

  class Process
  {
  public:
    template<typename...Args>
    Process(const std::string& program, Args&&... args) :
      m_program{program}
    {
      Fork();

      if(IsChild())
      {
        execlp(m_program.c_str(), m_program.c_str(), GetString(args)..., NULL);
        exit(125);
      }
    }

    Process(const std::string& program, const std::vector<std::string>& args);

    ~Process();

    static constexpr auto NO_HANDLER = [](){};
    
    template<typename ErrorHandler = decltype(NO_HANDLER)>
    static void CreateAndWait(const std::string& fileName, const std::vector<std::string>& args, ErrorHandler onErr = NO_HANDLER)
    {
      auto process = Process(fileName, args);
      if(auto ret = process.ReturnValue(); ret != 0)
      {
        process.LogOutput();
        process.LogErrors();

        onErr();

        std::string argStr{};
	for(auto&& arg : args)
	{
          argStr += arg + " ";
	}
	      
        throw std::runtime_error("Process " + fileName + " with args " + argStr + " returned " + std::to_string(ret));
      }

      process.LogOutput();
    }

    int ReturnValue();

    template<typename Logger = Logging::Logger<Logging::Formatter>::FlushGuard>
    void LogErrors(std::optional<std::reference_wrapper<Logger>> inLogger = std::nullopt)
    {
      static const auto ErrorAnnounceString = "LOGGING PROCESS ERRORS";
      auto&& errMsg = m_stdErr.Read();
      auto&& logger = inLogger ? inLogger->get().Log(ErrorAnnounceString) : Logging::ErrorLog(ErrorAnnounceString);
      logger.Log(errMsg);
    }

    template<typename Logger = Logging::Logger<Logging::Formatter>::FlushGuard>
    void LogOutput(std::optional<std::reference_wrapper<Logger>> inLogger = std::nullopt)
    {
      static const auto OutputAnnounceString = "LOGGING PROCESS OUTPUT";
      auto&& outMsg = m_stdOut.Read();
      auto&& logger = inLogger ? inLogger->get().Log(OutputAnnounceString) : Logging::ColorLog(OutputAnnounceString, Logging::Colors::GREEN);
      logger.Log(outMsg);
    }

    bool IsChild() const;

    bool IsExited();

  private:
    Pipe m_stdOut{};
    Pipe m_stdErr{};
    std::string m_program;

    ssize_t m_pid{};
    int32_t m_stats{};

    void Fork();

    void EnsureNotChild() const;

    void UpdateStats();
  };
}
