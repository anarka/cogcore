#pragma once
#include <sys/socket.h>
#include <functional>

template<typename EnumClass>
auto AsUnderlying(EnumClass&& value)
{
  return static_cast<std::underlying_type_t<EnumClass>>(value);
}

namespace Cog::System
{
  class Socket
  {
  public:
    enum class Domain
    {
      Local = AF_LOCAL,
      IPv4 = AF_INET,
      IPv6 = AF_INET6
    };

    Socket(Domain domain)
      : myHandle{socket(AsUnderlying(domain), 0, 0)}
    {}
  private:
    int myHandle;

  };
};
