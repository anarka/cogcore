#pragma once
#include <cstdlib>
#include <string>
#include <unordered_map>

namespace Cog::System
{
  class Variables
  {
  public:
    static constexpr auto NULL_STR = [](){return "";};

    template<typename OrFunctor = decltype(NULL_STR)>
    static std::string GetVariableOr(const std::string& envVarKey, OrFunctor&& func = NULL_STR)
    {
      auto envVar = std::getenv(envVarKey.c_str());
      
      return envVar != nullptr ? envVar : func();
    }

    static void Set(const std::string& name, const std::string& value)
    {
      static std::unordered_map<std::string, std::string> sSetVars{};
      auto position = sSetVars.find(name);
      
      if (position == sSetVars.end())
      {
        position = sSetVars.emplace(name, name + "=" + value).first;
      }

      putenv(position->second.data());
    }
  };
}
