#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>

#include <string_view>

#include <stdexcept>

#include "Pipe.h"

namespace Cog::System
{

  Pipe::Pipe()
  {
    OpenPipe();
  }
 
  Pipe::~Pipe()  
  {
    CloseAll();
  }

   
  void Pipe::WriteTo(int handle)
  {
    if(dup2(m_pipe[1], handle) != handle)
    {
      throw std::runtime_error("Failed to copy file handle to pipe");
    }
  }

  std::string Pipe::Read()
  {
    std::array<char, 1024> buffer{};
    std::string out{};
    std::string_view newlineSearcher{m_pipeOutput};

    size_t end{};
      
    auto newLinePos = [&newlineSearcher, &end]()
    {
      return newlineSearcher.find('\n');
    };
  
    while(newLinePos() == std::string::npos)
    {
      end = m_pipeOutput.size();

      auto readBytes = read(m_pipe[0], buffer.data(), buffer.size());

      if(readBytes == -1)
      {
        break;
      }

      if(readBytes > 0)
      {
        m_pipeOutput += std::string{buffer.begin(), buffer.begin() + readBytes};

        if(readBytes < (ssize_t)buffer.size())
        {
          break;
        }
      }
    }

    out = m_pipeOutput.substr(0, newLinePos());
    m_pipeOutput = m_pipeOutput.substr(newLinePos() + 1);

    return out;
  }
  
  void Pipe::CloseWrite()
  {
    close(m_pipe[1]);
  }

   
  void Pipe::CloseRead()
  {
    close(m_pipe[0]);
  }
   
  void Pipe::CloseAll()
  {
    CloseRead();
    CloseWrite();
  }

  void Pipe::OpenPipe()
  {
    if(pipe2(m_pipe.data(), O_CLOEXEC | O_NONBLOCK) != 0)
    {
      throw std::runtime_error("Pipe failed to open!");
    }
  }

}
