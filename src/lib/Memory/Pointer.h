#pragma once
#include <stddef.h>

namespace Cog::Memory
{

  template<typename AllocatedType>
  class DefaultAllocator
  {
  public:
    using AllocationIdType = size_t;

    static bool Validate(AllocationIdType allocationId, AllocatedType* allocation)
    {
      
    }

  private:
    size_t size{};
  };


  template<typename ReferedType, template <typename> typename AllocatorType = DefaultAllocator>
  class Reference
  {
    using Allocator = AllocatorType<ReferedType>;
  public:

    ReferedType& operator()() const
    {
      Validate();

      return *m_pointer;
    }

  private:
    ReferedType* m_pointer{};
    typename Allocator::AllocationIdType m_allocation{};

    void Validate() const
    {    
#ifndef NOMEMDEBUG
      if (!Allocator::Validate(m_allocation, m_pointer))
      {
        throw "Bad reference";
      }
#endif
    }

  };
};
