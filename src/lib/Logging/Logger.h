#pragma once

#include <features.h>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <string>
#include <cstring>

namespace Cog::Functional
{
  template<typename Func, typename...Args>
  auto Call(Func&&, Args&&...);
  
  template<typename Func, typename This, typename...Args>
  auto OnThis(Func&&, This&&, Args&&...);
}

namespace Cog::Filesystem
{
  class File;
}

namespace Cog::Logging
{

  enum class Colors
  {
    WHITE,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN
  };
  
  class ColoredSegment
  {
  public:
    ColoredSegment(const std::string& segment, Colors color = Colors::WHITE) :
      m_segment{ColorString(color) + segment + ColorString(Colors::WHITE)}
    {}

    const std::string& operator()() const
    {
      return m_segment;
    }

  private:
    std::string m_segment;

    static constexpr const char * ColorString(Colors color)
    {
      constexpr auto WHITE = "\x1B[0m";
      constexpr auto RED = "\x1B[31m";
      constexpr auto GREEN = "\x1B[32m";
      constexpr auto YELLOW = "\x1B[33m";
      constexpr auto BLUE = "\x1B[34m";
      constexpr auto MAGENTA = "\x1B[35m";
      constexpr auto CYAN = "\x1B[36m";

      switch(color)
      {
        case Colors::RED:
          return RED;
        case Colors::GREEN:
          return GREEN;
        case Colors::YELLOW:
          return YELLOW;
        case Colors::BLUE:
          return BLUE;
        case Colors::MAGENTA:
          return MAGENTA;
        case Colors::CYAN:
          return CYAN;
        case Colors::WHITE:
        default:
          return WHITE;
      }
    };

  };

  template<typename T>
  constexpr decltype(std::declval<T>().ToString(), true) HasToString(int)
  {
    return true;
  }

  template<typename>
  constexpr bool HasToString(...)
  {
    return false;
  } 

  template<typename T>
  auto Unpack(T&& arg)
  {
    if constexpr(HasToString<T>(0))
    {
      return arg.ToString();
    }
    else if constexpr(std::is_same_v<std::decay_t<T>, std::string>)
    {
      return arg.c_str();
    } 
    else if constexpr(std::is_same_v<std::decay_t<T>, std::string_view>)
    {
      return arg.data();
    }
    else if constexpr(std::is_convertible_v<const char*, T> || std::is_same_v<const char*, T>)
    {
      return arg;
    }
    else if constexpr(std::is_constructible_v<std::string, T>)
    {
      return std::string{std::forward<T>(arg)};
    }
    else
    {
      // printf args should match
      return arg;
    }
  }

  template<typename Guarded>
  class PerThread
  {
  public:
    Guarded& Get() const
    {
      return m_threadedItems[std::this_thread::get_id()];
    }

  private:
    mutable std::unordered_map<std::thread::id, Guarded> m_threadedItems{std::thread::hardware_concurrency()};
  };

  static constexpr auto RETURN = [](auto&& value) { return std::forward<decltype(value)>(value); };
  
  template<typename ToSanitize>
  auto Sanatize(ToSanitize&& in)
  {
    if constexpr(std::is_same_v<std::decay_t<ToSanitize>, std::string>)
    {
      std::replace(in.begin(), in.end(), '%', '_');
      return std::forward<ToSanitize>(in);
    }
    else
    {
      return std::forward<ToSanitize>(in);
    }
  }

  class Formatter
  {
  public:
    static constexpr size_t BUFFER_SIZE = 1024 * 1024;
  
    static Formatter& Get()
    {
      static std::mutex getGuard{};
      static PerThread<Formatter> formatters{};

      std::unique_lock<std::mutex> lk{getGuard};
      return formatters.Get();
    }

    template<typename FlushFunc, typename...Args>
    auto Format(const std::string& fmt, FlushFunc&& flushFunc, Args&&...args)
    {
      int charsFormatted;
      std::string out{};
      if (charsFormatted = snprintf(m_formatBuffer.data(), m_formatBuffer.size(), fmt.c_str(), Unpack(Unpack(args))...); charsFormatted < 0) 
      {
        throw std::runtime_error("Unhandled formatting error!");   
      }
      else if ((size_t)charsFormatted > m_formatBuffer.size())
      {
        throw std::runtime_error("Formatted message was larger than format buffer!");
      }
      
      out.resize(charsFormatted);
      std::memcpy(out.data(), m_formatBuffer.data(), charsFormatted);
      return flushFunc(std::move(Sanatize(out)));
      
    }

  private:
    std::array<char, BUFFER_SIZE> m_formatBuffer{}; 
  };


  class TimeString
  {
  public:

  private:

  };

  template<typename FormatterType>
  class Logger
  {
  public:
    inline static size_t LOGGER_ID = 0;
    static constexpr size_t BUFFER_SIZE = 100 * 1024;
   
    using Formatter = FormatterType;

    const size_t MY_ID = LOGGER_ID++;
    
    Logger()
    {
      m_outputBuffer.reserve(BUFFER_SIZE);
    }

    ~Logger()
    {
    }
 
    class FlushGuard
    {
    #ifndef NO_FROM_LOGGING
      static constexpr auto CUR_FILE_PREFIX = " %*s %s";
    #else
      static constexpr auto CUR_FILE_PREFIX = " %*s";
    #endif
    
    #ifndef NDEBUG
      static constexpr auto FILE_FROM_BUFFER = 95;
    #else
      static constexpr auto FILE_FROM_BUFFER = 80;
    #endif
    public:
      template<typename...Args>
      FlushGuard(Logger& toFlush, Colors color, FILE* outputHandle, const std::string& logType, const std::string& fmt, Args&&...args)
      : m_myLogger{toFlush},
        m_guardColor{color},
        m_fileHandle{outputHandle}
      {
        static constexpr auto LOG_HEADER = "[Thread %3d] [%s] [%s]: ";
        auto coloredString = ColoredSegment(fmt, m_guardColor); 
        
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
    
        struct tm currentLocalTime;
        localtime_r(&currentTime, &currentLocalTime);
        std::string timeBuffer{};
        timeBuffer.resize(18);

        std::strftime(timeBuffer.data(), 18, "%D %T", &currentLocalTime);

        auto headerStr = Formatter::Get().Format(LOG_HEADER, RETURN, m_myLogger.MY_ID, timeBuffer, logType);

        m_myLogger.Log(Functional::OnThis(&FlushGuard::OutputLog, this), headerStr + coloredString() + CUR_FILE_PREFIX + '\n', FILE_FROM_BUFFER - fmt.length(), "", std::forward<Args>(args)...);

        m_logSpacing = headerStr.length();//strlen(LOG_HEADER) + timeBuffer.length() + 2; 
      }

      template<typename...Args>
      FlushGuard& LogImpl(const std::string& fmt, Args&&...args)
      { 
        auto coloredString = ColoredSegment(fmt, m_guardColor); 
        
        m_myLogger.Log(Functional::OnThis(&FlushGuard::OutputLog, this),"%-*s  " + coloredString() + CUR_FILE_PREFIX + '\n', m_logSpacing, "", FILE_FROM_BUFFER - fmt.length() - 2, "", std::forward<Args>(args)...);
        return *this;
      }
      
      template<typename...Args>
      FlushGuard& LogImpl(Colors overrideColor, const std::string& fmt, Args&&...args)
      { 
        auto coloredString = ColoredSegment(fmt, overrideColor); 
        
        m_myLogger.Log(Functional::OnThis(&FlushGuard::OutputLog, this),"%-*s  " + coloredString() + CUR_FILE_PREFIX + '\n', m_logSpacing, "", FILE_FROM_BUFFER - fmt.length() - 2, "", std::forward<Args>(args)...);
        return *this;
      }

      void OutputLog(std::string&& outputString)
      {
        fputs(outputString.c_str(), m_fileHandle);
      }

      ~FlushGuard()
      {
        m_myLogger.Flush(Functional::OnThis(&FlushGuard::OutputLog, this));
      }

    private:
      Logger& m_myLogger;
      Colors m_guardColor;
      FILE* m_fileHandle{stdout};
      size_t m_logSpacing{};
      std::string m_fromLinePrefix{};
    };

    template<typename...Args>
    FlushGuard GetFlushGuard(Colors color, FILE* outputFile, const std::string& logType, const std::string& fmt, Args&&...args)
    {
      return FlushGuard(*this, color, outputFile, logType, fmt, std::forward<Args>(args)...);
    }

  private:
    template<typename FlushCallback, typename...Args>
    Logger& Log(FlushCallback&& flushFunc, const std::string& fmt, Args&&...args)
    {
      auto&& FormatCallback = [this, &flushFunc](std::string&& formatted)
      {
        if (formatted.size() > m_outputBuffer.capacity() - m_outputBuffer.size())
        {
          Functional::Call(std::forward<FlushCallback>(flushFunc), std::move(m_outputBuffer));
          m_outputBuffer.clear();
          m_outputBuffer.reserve(BUFFER_SIZE);
        }

        m_outputBuffer += std::move(formatted);
      };

      m_formatter.Format(fmt, FormatCallback, std::forward<Args>(args)...);

      return *this;
    }

    template<typename FlushCallback, typename...Args>
    void Flush(FlushCallback&& flushFunc, Args&&...args)
    {
      Functional::Call(std::forward<FlushCallback>(flushFunc), std::move(m_outputBuffer), std::forward<Args>(args)...);
      m_outputBuffer.clear();
      m_outputBuffer.reserve(BUFFER_SIZE);
    }

    Formatter& m_formatter{Formatter::Get()};

    std::string m_outputBuffer{};
    
    uint64_t m_bytesLogged{};
  };

  template<typename...Args>
  std::string FormatString(const std::string& fmt, Args&&...args)
  {
    return Formatter::Get().Format(fmt, RETURN, std::forward<Args>(args)...);
  }
 
  class ThreadedLogger
  {
  public:
    inline static PerThread<Logger<Formatter>> logger{};
  };

  template<typename...Args>
  Logger<Formatter>::FlushGuard ColorLogImpl(const std::string& fmt, Colors color, Args&&... args)
  { 
    return ThreadedLogger::logger.Get().GetFlushGuard(color, stdout, "Color", fmt, std::forward<Args>(args)...);
  }

#ifndef NO_FROM_LOGGING
  #define SOURCE_DATA_STRING \
   , Cog::Logging::FormatString("from %-20s %-20s [Line: %4d]", "[" + Cog::Filesystem::File{__FILE__}.Fullname() + "]","[" +  std::string{__func__} + "]", __LINE__)
#else
  #define SOURCE_DATA_STRING 
#endif

#define Log(FMT_STR, ...) \
  LogImpl(Cog::Logging::FormatString(FMT_STR, Cog::Logging::RETURN __VA_OPT__(,) __VA_ARGS__) SOURCE_DATA_STRING)

#define LogColor(COLOR, FMT_STR, ...) \
  LogImpl(COLOR, Cog::Logging::FormatString(FMT_STR, Cog::Logging::RETURN __VA_OPT__(,) __VA_ARGS__) SOURCE_DATA_STRING)

#define ColorLog(FMT_STR, COLOR, ...) \
  ColorLogImpl(Cog::Logging::FormatString(FMT_STR, Cog::Logging::RETURN __VA_OPT__(,) __VA_ARGS__), COLOR  SOURCE_DATA_STRING)

  template<typename...Args>
  auto ErrorLogImpl(const std::string& fmt, Args&&...args)
  {
    auto coloredString = ColoredSegment(fmt, Colors::RED);

    return ThreadedLogger::logger.Get().GetFlushGuard(Colors::RED, stderr, "Error", fmt, std::forward<Args>(args)...);
  }

#define ErrorLog(FMT_STR, ...) \
  ErrorLogImpl(Cog::Logging::FormatString(FMT_STR, Cog::Logging::RETURN __VA_OPT__(,) __VA_ARGS__) SOURCE_DATA_STRING)

  template<typename...Args>
  auto DebugLogImpl(const std::string& fmt, Args&&...args)
  {
#ifndef NDEBUG
    return ColorLogImpl(fmt, Colors::YELLOW, std::forward<Args>(args)...);
#else
    static constexpr auto eat = [](auto&&...){};
    (void)fmt;
    eat(args...);
#endif
  }

#define DebugLog(FMT_STR, ...) \
  DebugLogImpl(Cog::Logging::FormatString(FMT_STR __VA_OPT__(,) __VA_ARGS__) SOURCE_DATA_STRING)
}

#include <CogCore/File.h>
#include <CogCore/FunctionWrappers.h>
