#include <CogCore/App.h>
#include <CogCore/DynamicFunction.h>
#include <CogCore/Variables.h>
#include <CogCore/Process.h>

CogMain
  (void)(args);
  System::Variables::Set("LD_LIBRARY", ".");
  struct BadBuilder
  {
    static Filesystem::File Build(const Filesystem::File& sourceFile, const Filesystem::File& outputDir)
    {
      Filesystem::File outFile = sourceFile.WithExt(".so").LocatedAt(outputDir);

      if (sourceFile.Timestamp() > outFile.Timestamp())
      {
        System::Process::CreateAndWait("g++-10", {"-I./build/include/c++", sourceFile.Path(), "-g", "-shared", "-rdynamic", "-Wl,-soname,test.so.1", "-o" + outFile.Path(), "-std=c++2a", "-fPIC", "-lstdc++fs", "./build/lib/libCogCore.a"});
      }

      return outFile;
    }
  };

  Functional::JITObject<BadBuilder> toLoad{Filesystem::File{"test.cpp"}, Filesystem::File{"./"}};
  return toLoad.myTargetObject.GetFunction<int(*)()>("yay")();
EndMain
