#include <CogCore/ConfigFile.h>
#include <string>

struct TestVar1
{
  static constexpr auto KEY = "TEST";
  using ValueType = std::string;
  inline static const std::string DEFAULT = "Yey";

  /*static auto Convert(std::string_view parsedValue)
  {
    (void)parsedValue;
    return std::string{};
  }*/

  static auto Append(std::remove_cv_t<ValueType>& toAppend, ValueType&&)
  {
    toAppend += "Test";
  }

  static auto Finalize(std::remove_cv_t<ValueType>& toFinalize, Cog::Config::ConfigFile&)
  {
    toFinalize += "Boo";
  }
};

NAMESPACED_COG(Test)
  NEW_COG_SIMPLE(A, "")
  NEW_COG_SIMPLE(B, "")
END_COG()

int main()
{
  try
  {
   Cog::Config::ConfigFile testCog{"test"};
   Cog::Config::ConfigFile::Value<TestVar1> testVar{&testCog};
   testVar.Set("a");

   auto& testA = TestCog::Get("a");
   auto& testB = TestCog::Get("b");
   auto logger = Cog::Logging::ErrorLog("TestCog namespace %s value A %s - value B %s", testA.Namespace(), testA.A(), testA.B()); 
   
   logger.Log("TestCog namespace %s value A %s - value B %s", testB.Namespace(), testB.A(), testB.B()); 

   printf("%s\n", testVar().c_str());

   Cog::Config::Test test{};
  }
  catch(const char* e)
  {
    printf("%s", e);
  }
}
