#include <CogCore/Fork.h>
#include <CogCore/Logger.h>

int main()
{
  Cog::Logging::ColorLog("Starting Fork test", Cog::Logging::Colors::GREEN);
  auto test = Cog::System::Fork{[](){}};

  test.Wait();
  Cog::Logging::ColorLog("%d", Cog::Logging::Colors::GREEN, test.ReturnValue());

  if(test.ReturnValue() == Cog::System::Fork::Cog_UNKNOWN_EXIT)
  {
    Cog::Logging::ColorLog("Yay", Cog::Logging::Colors::GREEN);
    return 0;
  }
  else
  {
    Cog::Logging::ErrorLog("Boo");
    return 1;
  }
}
