#!/bin/bash
rm -r build
shopt -s globstar
mkdir -p build/include/CogCore
cp src/**/*.h build/include/CogCore
g++-8 -c -Ibuild/include -I/home/arkos/.local/share/FAF/include src/lib/**/*.cpp -std=c++2a -lpthread -lstdc++fs -O3
g++-8 -Ibuild/include -I/home/arkos/.local/share/FAF/include src/bin/Drivers/FAFbuild.cpp *.o -std=c++2a /home/arkos/.local/share/FAF/CogCore/libCogCore.a -otest -lpthread -lstdc++fs -O3
